import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;


public class Excel extends JFrame implements ActionListener{
	private JFrame frame;
	private JTable table;
	private JTextField text;
	private JScrollPane sp;
	private JPanel p;
	private JCheckBox ckbox = new JCheckBox();
	int i=0;
	
	public static void main(String[] args){
		Excel excel = new Excel("Excel");
		excel.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		excel.setVisible(true);
		}
	Excel(String title){
		setTitle(title);
		setBounds(0,0,800,800);//表示位置とウィンドウサイズ
		
		//メニューバー作成
		JMenuBar menubar = new JMenuBar();
	    JMenu menu1 = new JMenu("File");
	    JMenu menu2 = new JMenu("Edit");
	    menubar.add(menu1);
	    menubar.add(menu2);
	    JMenuItem menuitem1 = new JMenuItem("セーブ");
	    JMenuItem menuitem2 = new JMenuItem("ロード");
	    JMenuItem menuitem3 = new JMenuItem("縦表示");
	    JMenuItem menuitem4 = new JMenuItem("横表示");
	    JMenuItem menuitem5 = new JMenuItem("セル連結");
	    JMenuItem menuitem6 = new JMenuItem("セル解除");
	    JMenuItem menuitem7 = new JMenuItem("罫線作成");
	    JMenuItem menuitem8 = new JMenuItem("チェックボックス機能");
	    JMenuItem menuitem9 = new JMenuItem("範囲指定");
	    
	    menu1.add(menuitem1);
	    menu1.add(menuitem2);
	    menu2.add(menuitem3);
	    menu2.add(menuitem4);
	    menu2.add(menuitem5);
	    menu2.add(menuitem6);
	    menu2.add(menuitem7);
	    menu2.add(menuitem8);
	    menu2.add(menuitem9);
	    setJMenuBar(menubar);
	    menuitem1.addActionListener(this);
	    menuitem2.addActionListener(this);
	    menuitem3.addActionListener(this);
	    menuitem4.addActionListener(this);
	    menuitem5.addActionListener(this);
	    menuitem6.addActionListener(this);
	    menuitem7.addActionListener(this);
	    menuitem8.addActionListener(this);
	    menuitem9.addActionListener(this);
		
	    //セル作成
		table = new JTable(99,99);//セルサイズ
		table.setFont(new Font("Arial", Font.PLAIN, 12));//フォント指定
		table.setRowSelectionAllowed(true);//セルの行の選択状態を変更
		table.setColumnSelectionAllowed(true);//セルの列の選択状態を変更
		//table.setGridColor(Color.BLACK);//グリット線
		DefaultTableModel tableModel
	     = new DefaultTableModel(0,0);
		
		//テキストフィールド作成
	    text = new JTextField(40);//テキストフィールド作成
	    
	    text.setFont(new Font("Arial", Font.PLAIN, 12));//フォント指定
	    text.setHorizontalAlignment(JTextField.LEFT);//テキストの文字列内配置指定
	    
	    //ボタン
	    JButton button = new JButton("セル入力");
	    button.addActionListener(this);
	    	    
	    //セル幅指定
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	    DefaultTableColumnModel columnModel
	    = (DefaultTableColumnModel)table.getColumnModel();

	    TableColumn column=null;
	    for (int i=0;i<columnModel.getColumnCount();i++){
	    column = columnModel.getColumn(i);
	    column.setPreferredWidth(2);
	    }
	    
	    //チェックボックス機能
	    JCheckBox ckbox = new JCheckBox();
	    ckbox.addActionListener(this);
	    
		sp = new JScrollPane(table);
	    sp.setPreferredSize(new Dimension(650,650));//フレームサイズ(x,y)
	    
	    p = new JPanel();
	    p.add(sp);
	    p.add(text);
	    p.add(button);
	    //p.add(ckbox);

	    Container contentPane = getContentPane();
	    contentPane.add(p, BorderLayout.SOUTH);
	    getContentPane().add(p,BorderLayout.CENTER);
	}
	
	public void actionPerformed(ActionEvent e) {
		// TODO 自動生成されたメソッド・スタブ
		//セル入力
		if (e.getActionCommand()=="セル入力"){
			for(int i=0;i<text.getText().length();i++){
				table.setValueAt(text.getText().substring(i, i+1),table.getSelectedRow(),table.getSelectedColumn()+i);//セルに文字が追加される
				//デフォルトでは横に表示される
			}
		}
		
		//縦横変換
		//縦表示
		if (e.getActionCommand() == "縦表示"){
			for(int i=0;i<text.getText().length();i++){
				table.setValueAt(text.getText().substring(i, i+1),table.getSelectedRow()+i,table.getSelectedColumn());//セルに文字が横に追加される
			}
		}
		//横表示
		if (e.getActionCommand() == "横表示"){
			for(int i=0;i<text.getText().length();i++){
				table.setValueAt(text.getText().substring(i, i+1),table.getSelectedRow(),table.getSelectedColumn()+i);//セルに文字が縦に追加される
			}
		}
	
		//セル連結
		if (e.getActionCommand()=="セル連結"){
			//セルの座標取得
			table.addMouseListener(new java.awt.event.MouseAdapter()  {
				public void mouseClicked(MouseEvent e) {
					// 選択セルの座標を取得する
					for(i=0;i<10;i++){
						int row = table.getSelectedRow()+i;
						int col = table.getSelectedColumn()+i;
						//System.out.println("行" + row + "::" + "列" + col);
					}
						//セル連結させる
						int gettext=text.getText().length();
						int row1=(table.getSelectedRow()+i)-table.getSelectedRow();//個数を取得
						int col1=(table.getSelectedColumn()+i)-table.getSelectedColumn();//個数を取得
						System.out.println("行" + row1 + "::" + "列" + col1);
						System.out.println("文字数:"+gettext);	
				   }
				  });
		}
		
		//セル解除
		if(e.getActionCommand()=="セル解除"){
			//セルの座標取得
			table.addMouseListener(new java.awt.event.MouseAdapter()  {
				public void mouseClicked(MouseEvent e) {
					// 選択セルの座標を取得する
						int row = table.getSelectedRow()+i;
						int col = table.getSelectedColumn()+i;
						//System.out.println("行" + row + "::" + "列" + col);
				}
				
			});
		}
		
		//セーブ機能
		if (e.getActionCommand() == "セーブ"){
			
        }
		
		//ロード機能
		if (e.getActionCommand() == "ロード"){
			
        }
		
		//罫線作成
		if (e.getActionCommand() == "罫線作成"){
			
		}
		
		
		//チェックボックス機能
		if (e.getActionCommand() == "チェックボックス機能"){
			//セルの座標取得
			table.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					// 選択セルの座標を取得する
					int row = table.getSelectedRow();
					int col = table.getSelectedColumn();
				    System.out.println("行" + row + "::" + "列" + col);
					//セルにチェックボックスを表示させる
					table.setValueAt("✔",table.getSelectedRow(),table.getSelectedColumn());
				   }
				  });
        }
		
		//範囲指定
		if (e.getActionCommand() == "範囲指定"){
			table.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					int number=0;
					for(number=0;number<2;number++){
						if(number==0){
							//1回目:セルの座標取得
							//選択セルの座標を取得する
							int row1 = table.getSelectedRow();
							int col1 = table.getSelectedColumn();
							System.out.println("1回目:"+"行" + row1 + "::" + "列" + col1);
						}
						if(number==1){
							//2回目:セルの座標取得
							// 選択セルの座標を取得する
							int row2 = table.getSelectedRow();
							int col2 = table.getSelectedColumn();
							System.out.println("2回目:"+"行" + row2 + "::" + "列" + col2);
						}
				}
				}
				});
        }	
	}
}