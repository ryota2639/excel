package Excel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Label;
import java.awt.Menu;
import java.awt.MenuItem;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class Excel extends JFrame implements ActionListener{
	private JFrame frame;
	private JTable table;
	private JTextField text,text1,text2;
	//private JCheckBox checkbox;
	private JScrollPane sp;
	private JPanel p;
	private int i=0,flag;
	private int row1,col1,row2,col2,row3,col3;;

	CellRenderer CR = new CellRenderer();

	public static void main(String[] args){
		Excel excel = new Excel("Excel");
		excel.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		excel.setVisible(true);
	}
	Excel(String title){
		setTitle(title);
		setBounds(0,0,800,800);//表示位置とウィンドウサイズ


		//メニューバー作成
		JMenuBar menubar = new JMenuBar();
	    JMenu file = new JMenu("ファイル");
	    JMenu edit = new JMenu("編集");
	    JMenu display = new JMenu("表示");
	    JMenu help = new JMenu("ヘルプ");
	    JMenu deconcatenation = new JMenu("連結・解除");
	    JMenu item = new JMenu("アイテム");
	    JMenuItem save = new JMenuItem("セーブ");
	    JMenuItem load = new JMenuItem("ロード");
	    JMenuItem verticaldisplay = new JMenuItem("縦表示");
	    JMenuItem horizontaldisplay = new JMenuItem("横表示");
	    JMenuItem cellconnected = new JMenuItem("セル連結");
	    JMenuItem cellrelease = new JMenuItem("セル解除");
	    JMenuItem borderscreated = new JMenuItem("罫線作成");
	    JMenuItem checkbox = new JMenuItem("チェックボックス");
	    JMenuItem rangespecification = new JMenuItem("範囲指定");
	    JMenuItem version=new JMenuItem("バージョン情報");

	    //メニューバー表示
	    setJMenuBar(menubar);
	    menubar.add(file);
	    menubar.add(edit);
	    menubar.add(help);
	    file.add(save);
	    file.add(load);
	    edit.add(display);
	    edit.add(deconcatenation);
	    edit.add(item);
	    display.add(verticaldisplay);
	    display.add(horizontaldisplay);
	    deconcatenation.add(cellconnected);
	    deconcatenation.add(cellrelease);
	    item.add(borderscreated);
	    item.add(checkbox);
	    item.add(rangespecification);
	    help.add(version);

	    //メニューバー監視
	    save.addActionListener(this);
	    load.addActionListener(this);
	    verticaldisplay.addActionListener(this);
	    horizontaldisplay.addActionListener(this);
	    cellconnected.addActionListener(this);
	    cellrelease.addActionListener(this);
	    borderscreated.addActionListener(this);
	    checkbox.addActionListener(this);
	    rangespecification.addActionListener(this);
	    version.addActionListener(this);

	    //セル作成
		table = new JTable(99,99);//セル個数
		table.setFont(new Font("Arial", Font.PLAIN, 12));//フォント指定
		table.setRowSelectionAllowed(true);//セルの行の選択状態を変更
		table.setColumnSelectionAllowed(true);//セルの列の選択状態を変更
		//table.setGridColor(Color.BLACK);//グリット線
		DefaultTableModel tableModel
	     = new DefaultTableModel(0,0);

		//テキストフィールド作成
	    text = new JTextField(40);//テキストフィールド作成
	    text1 = new JTextField(10);//テキストフィールド作成
	    text2 = new JTextField(10);//テキストフィールド作成

	    text.setFont(new Font("Arial", Font.PLAIN, 12));//フォント指定
	    text1.setFont(new Font("Arial", Font.PLAIN, 12));//フォント指定
	    text2.setFont(new Font("Arial", Font.PLAIN, 12));//フォント指定

	    text.setHorizontalAlignment(JTextField.LEFT);//テキストの文字列内配置指定
	    text1.setHorizontalAlignment(JTextField.LEFT);//テキストの文字列内配置指定
	    text2.setHorizontalAlignment(JTextField.LEFT);//テキストの文字列内配置指定

	    //ボタン
	    JButton button = new JButton("セル入力");
	    JButton button1 = new JButton("属性入力");
	    JButton button2 = new JButton("属性名非表示");
	    JButton button3 = new JButton("属性値入力");

	    //ボタン監視
	    button.addActionListener(this);
	    button1.addActionListener(this);
	    button2.addActionListener(this);
	    button3.addActionListener(this);

	    //セル幅指定
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	    DefaultTableColumnModel columnModel
	    = (DefaultTableColumnModel)table.getColumnModel();

	    TableColumn column=null;
	    for (int i=0;i<columnModel.getColumnCount();i++){
	    column = columnModel.getColumn(i);
	    column.setPreferredWidth(2);
	    }

	    //ラベル作成
	    JLabel zokusei = new JLabel("属性:");
	    JLabel zokuseiti = new JLabel("属性値:");

	    //table.setDefaultRenderer(Object.class, new CellRenderer());

	    //zokusei.setBounds(500,100,10,30);
	    //zokuseiti.setBounds(510,30,10,30);


	    //table.getCellRect(row, column, includeSpacing)
	    //チェックボックス機能
	    //JCheckBox checkbox = new JCheckBox();
	    //checkbox.addActionListener(this);

		sp = new JScrollPane(table);
	    sp.setPreferredSize(new Dimension(775,650));//フレームサイズ(x,y)

	    p = new JPanel();
	    //p.setLayout(null);
	    p.add(sp);

	    //p.add(text);
	    //p.add(button);
	    p.add(zokusei);
	    p.add(text1);
	    p.add(button1);
	    p.add(button2);
	    p.add(zokuseiti);
	    p.add(text2);
	    p.add(button3);
	    //p.add(ckbox);

	    Container contentPane = getContentPane();
	    contentPane.add(p, BorderLayout.CENTER);
	    //getContentPane().add(p,BorderLayout.CENTER);

	}

	public void actionPerformed(ActionEvent e) {
		// TODO 自動生成されたメソッド・スタブ
		//セル入力(ボタン)
		if (e.getActionCommand()=="セル入力"){
			for(int i=0;i<text.getText().length();i++){
				table.setValueAt(text.getText().substring(i, i+1),table.getSelectedRow(),table.getSelectedColumn()+i);
				//セルに文字が追加される
				//デフォルトでは横に表示される
			}
		}

		//属性入力(ボタン)
		if (e.getActionCommand()=="属性入力"){
			for(int i=0;i<text1.getText().length();i++){
				table.setDefaultRenderer(Object.class, new CellRenderer());
				//getTableCellRendererComponent()
				table.setValueAt(text1.getText().substring(i, i+1),table.getSelectedRow(),table.getSelectedColumn()+i);
				//セルに文字が追加される
				//デフォルトではに表示される
			}
		}

		//属性名非表示(ボタン)
		if (e.getActionCommand()=="属性名非表示"){
			//textの中の文字列を非表示にする

		}

		//属性値入力(ボタン)
		if (e.getActionCommand()=="属性値入力"){
			for(int i=0;i<text2.getText().length();i++){
				table.setValueAt(text2.getText().substring(i, i+1),table.getSelectedRow(),table.getSelectedColumn()+i);
				//セルに文字が追加される
				//デフォルトでは横に表示される
			}
		}

		//縦横変換
		//縦表示
		if (e.getActionCommand() == "縦表示"){
			for(int i=0;i<text.getText().length();i++){
				//セルに文字が横に追加される
				table.setValueAt(text.getText().substring(i, i+1),table.getSelectedRow()+i,table.getSelectedColumn());
			}
		}
		//横表示
		if (e.getActionCommand() == "横表示"){
			for(int i=0;i<text.getText().length();i++){
				//セルに文字が縦に追加される
				table.setValueAt(text.getText().substring(i, i+1),table.getSelectedRow(),table.getSelectedColumn()+i);
			}
		}

		//セル連結
		if (e.getActionCommand()=="セル連結"){
			table.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					//クリック処理 2回クリックさせる
					if(flag==0){
						//1回目:セルの座標取得
						//選択セルの座標を取得する
						row1 = table.getSelectedRow();
						col1 = table.getSelectedColumn();
						System.out.println("1回目:"+"行" + row1 + "::" + "列" + col1);
						//flag=1;
					}
					else{
						//2回目:セルの座標取得
						// 選択セルの座標を取得する
						row2 = table.getSelectedRow();
						col2 = table.getSelectedColumn();
						System.out.println("2回目:"+"行" + row2 + "::" + "列" + col2);
						flag=0;
					}
					if(flag==0){
						int row=row2-row1;//個数を計算
						int col=col2-col1;//個数を計算
						//System.out.println(row+"::"+col);//個数表示
						for(int r=row1;r<=row1+row;r++){
							for(int c=col1;c<=col1+col;c++){
								//System.out.println(r+"::"+c);//座標表示
								//セルを連結させる

							}
						}
					}
					}
				});

		}

		//セル解除
		if(e.getActionCommand()=="セル解除"){
			table.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					//クリック処理 2回クリックさせる
					if(flag==0){
						//1回目:セルの座標取得
						//選択セルの座標を取得する
						row1 = table.getSelectedRow();
						col1 = table.getSelectedColumn();
						System.out.println("1回目:"+"行" + row1 + "::" + "列" + col1);
						flag=1;
					}else{
						//2回目:セルの座標取得
						// 選択セルの座標を取得する
						row2 = table.getSelectedRow();
						col2 = table.getSelectedColumn();
						System.out.println("2回目:"+"行" + row2 + "::" + "列" + col2);
						flag=0;
					}
					if(flag==0){
						int row=row2-row1;//個数を計算
						int col=col2-col1;//個数を計算
						//System.out.println(row+"::"+col);//個数表示
						for(int r=row1;r<=row+row1;r++){
							for(int c=col1;c<=col+col1;c++){
								//System.out.println(r+"::"+c);//座標表示
								//連結したセルを解除させる

							}
						}
					}
					}
				});
		}

		//セーブ機能
		if (e.getActionCommand() == "セーブ"){

        }

		//ロード機能
		if (e.getActionCommand() == "ロード"){

        }

		//罫線作成
		if (e.getActionCommand() == "罫線作成"){
			//範囲指定する
			//テーブルにマウスイベントをつけて監視する
			/*
			table.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					//クリック処理 2回クリックさせる
					if(flag==0){
						//1回目:セルの座標取得
						//選択セルの座標を取得する
						row1 = table.getSelectedRow();
						col1 = table.getSelectedColumn();
						System.out.println("1回目:"+"行" + row1 + "::" + "列" + col1);//1回目の座標を表示
						flag=1;
						}else{
							//2回目:セルの座標取得
							// 選択セルの座標を取得する
							row2 = table.getSelectedRow();
							col2 = table.getSelectedColumn();
							System.out.println("2回目:"+"行" + row2 + "::" + "列" + col2);//2回目の座標を表示
							flag=0;
						}
					if(flag==0){
						int row=row2-row1;//個数を計算
						int col=col2-col1;//個数を計算
						//System.out.println(row+"::"+col);//個数表示
						for(int r=row1;r<=row+row1;r++){
							for(int c=col1;c<=col+col1;c++){
								System.out.println(Math.abs(r)+"::"+Math.abs(c));//座標表示
								//罫線を作成する
							}
						}
					}
				}
			});*/


			//LineBorder border = new LineBorder(Color.RED, 2, true);
			//text2.setBorder(border);

			//セルの座標取得
			/*
			table.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					// 選択セルの座標を取得する
					int row = table.getSelectedRow();
					int col = table.getSelectedColumn();
					//System.out.println("行" + row + "::" + "列" + col);
					//セルに罫線を表示させる
					//table.setowHorizontalLines(true);//セル間に水平線を描画
					//table.setShowVerticalLines(true);//セル間に垂直線を描画
				   }
				  });
			*/
		}

		//チェックボックス機能
		if (e.getActionCommand() == "チェックボックス"){
			//セルの座標取得
			table.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					// 選択セルの座標を取得する
					int row = table.getSelectedRow();
					int col = table.getSelectedColumn();
					//System.out.println("行" + row + "::" + "列" + col);
					//セルにチェックボックスを表示させる
					table.setValueAt("✔︎",row,col);
				   }
				  });
        }

		//範囲指定
		if (e.getActionCommand() == "範囲指定"){
			table.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					//クリック処理 2回クリックさせる
					if(flag==0){
						//1回目:セルの座標取得
						//選択セルの座標を取得する
						row1 = table.getSelectedRow();
						col1 = table.getSelectedColumn();
						System.out.println("1回目:"+"行" + row1 + "::" + "列" + col1);
						flag=1;
						}else{
							//2回目:セルの座標取得
							//選択セルの座標を取得する
							row2 = table.getSelectedRow();
							col2 = table.getSelectedColumn();
							System.out.println("2回目:"+"行" + row2 + "::" + "列" + col2);
							flag=0;
						}
					if(flag==0){
						//row1→row2,col1→col2
						/*例：
						 * 行10,列10(1回目)
						 * 行30,列40(2回目)
						 */
						if(row1<row2&&col1<col2){
							int row=row2-row1;//個数を計算
							int col=col2-col1;//個数を計算
							//System.out.println("row個数:"+row+"::"+"col個数:"+col);//個数表示
							for(int r=row1;r<=row+row1;r++){
								for(int c=col1;c<=col+col1;c++){
									System.out.println(Math.abs(r)+"::"+Math.abs(c));//座標表示
									//table.setBackground(Color.RED);//範囲指定した分だけ色を変える
									//table.setSelectionBackground(Color.RED);
								}
							}
						}
						//row1→row2,col1→col2
						/*例：
						 * 行30,列40(1回目)
						 * 行10,列10(2回目)
						 */
						/*
						else if(row1>row2&&col1>col2){
							int row=row1-row2;//個数を計算
							int col=col1-col2;//個数を計算
							System.out.println(row+"::"+col);//個数表示
							for(int r=row1;r<=row-row1;r++){
								for(int c=col1;c<=col-col1;c++){
									System.out.println(r+"::"+c);//座標表示
									//table.setBackground(Color.RED);//範囲指定した分だけ色を変える
									//table.setSelectionBackground(Color.RED);
								}
							}
						}
						*/
						//row1→row2,col1→col2
						/*例：
						 * 行10,列20(1回目)
						 * 行30,列10(2回目)
						 */
						/*
						else if(row1<row2&&col1>col2){
							int row=row2-row1;//個数を計算
							int col=col2-col1;//個数を計算
							System.out.println(Math.abs(row)+"::"+col);//個数表示
							for(int r=row1;r<=row-row1;r--){
								for(int c=col1;c<=col1+col;c--){
									System.out.println(r+"::"+c);//座標表示
									//table.setBackground(Color.RED);//範囲指定した分だけ色を変える
									//table.setSelectionBackground(Color.RED);
								}
							}
						}
						*/
						//row1→row2,col1→col2
						/*例：
						 * 行30,列10(1回目)
						 * 行10,列20(2回目)
						 */
						/*
						else if(row1>row2&&col1<col2){
							int row=row2-row1;//個数を計算
							int col=col2-col1;//個数を計算
							System.out.println(row+"::"+col);//個数表示
							for(int r=row1;r<=row+row1;r++){
								for(int c=col1;c<=col+col1;c++){
									System.out.println(r+"::"+c);//座標表示
									//table.setBackground(Color.RED);//範囲指定した分だけ色を変える
									//table.setSelectionBackground(Color.RED);
								}
							}
						}*/
					}
				}
			});
		}
		if(e.getActionCommand()=="バージョン情報"){
			Dialog alert = new Dialog(frame, "バージョン情報");
			alert.add(new Label("version:2015/10/26"));
			alert.setSize(200,200);
			alert.setVisible(true);
			alert.addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent e){System.exit(0);}});
		}
	}
}

//JTableで各セルを描画(表示)するための標準クラス
class CellRenderer extends DefaultTableCellRenderer {
	public CellRenderer(){
		super();
	}
	//セルに描画
	public Component getTableCellRendererComponent(JTable table, Object value,boolean isSelected, boolean hasFocus,int row, int col) {
		super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
		setBackground(Color.red);//色を設定
		return this;
	}
}