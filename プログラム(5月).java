import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.table.TableColumn;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;


public class Excel extends JFrame implements ActionListener{
	private JTable table;
	private JTextField text;
	private JScrollPane sp;
	private JPanel p;
	private Container contentPane;
	private JFrame frame;
	private String ct[][]={};
	
	public static void main(String[] args){
		Excel excel = new Excel("Excel");
		excel.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		excel.setVisible(true);
		}
	Excel(String title){
		setTitle(title);
		setBounds(0,0,800,800);//表示位置とウィンドウサイズ
		
		//メニューバー作成
		JMenuBar menubar = new JMenuBar();
	    JMenu menu1 = new JMenu("File");
	    JMenu menu2 = new JMenu("Edit");
	    menubar.add(menu1);
	    menubar.add(menu2);
	    JMenuItem menuitem1 = new JMenuItem("セーブ");
	    JMenuItem menuitem2 = new JMenuItem("ロード");
	    JMenuItem menuitem3 = new JMenuItem("縦表示");
	    JMenuItem menuitem4 = new JMenuItem("横表示");
	    JMenuItem menuitem5 = new JMenuItem("セル連結");
	    JMenuItem menuitem6 = new JMenuItem("セル解除");
	    
	    menu1.add(menuitem1);
	    menu1.add(menuitem2);
	    menu2.add(menuitem3);
	    menu2.add(menuitem4);
	    menu2.add(menuitem5);
	    menu2.add(menuitem6);
	    setJMenuBar(menubar);
	    menuitem1.addActionListener(this);
	    menuitem2.addActionListener(this);
	    menuitem3.addActionListener(this);
	    menuitem4.addActionListener(this);
	    menuitem5.addActionListener(this);
	    menuitem6.addActionListener(this);
		
	    //セル作成
		table = new JTable(99,99);//セルサイズ
		table.setFont(new Font("Arial", Font.PLAIN, 12));//フォント指定
		table.setRowSelectionAllowed(true);//セルの行の選択状態を変更
		table.setColumnSelectionAllowed(true);//セルの列の選択状態を変更
		DefaultTableModel tableModel
	     = new DefaultTableModel(0,0);
		
		//テキストフィールド作成
	    text = new JTextField(40);//テキストフィールド作成
	    text.setFont(new Font("Arial", Font.PLAIN, 12));//フォント指定
	    text.setHorizontalAlignment(JTextField.LEFT);//テキストの文字列内配置指定
	    JButton button = new JButton("セル入力");
	    button.addActionListener(this);
	    text.addActionListener(this);
	    
	    //セル幅指定
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	    DefaultTableColumnModel columnModel
	    = (DefaultTableColumnModel)table.getColumnModel();

	    TableColumn column=null;
	    for (int i=0;i<columnModel.getColumnCount();i++){
	    column = columnModel.getColumn(i);
	    column.setPreferredWidth(2);
	    }
	    
		sp = new JScrollPane(table);
	    sp.setPreferredSize(new Dimension(650,650));//フレームサイズ(x,y)
	    
	    p = new JPanel();
	    p.add(sp);
	    p.add(text);
	    p.add(button);

	    Container contentPane = getContentPane();
	    contentPane.add(p, BorderLayout.SOUTH);
	    getContentPane().add(p,BorderLayout.CENTER);
	}
	
	public void actionPerformed(ActionEvent e) {
		// TODO 自動生成されたメソッド・スタブ
		//ボタンを押すとセルに入力される
		if (e.getActionCommand()=="セル入力"){
			for(int i=0;i<text.getText().length();i++){
				table.setValueAt(text.getText().substring(i, i+1),table.getSelectedRow(),table.getSelectedColumn()+i);
			}
		}
		//縦横変換
		if (e.getActionCommand() == "縦表示"){
			for(int i=0;i<text.getText().length();i++){
				table.setValueAt(text.getText().substring(i, i+1),table.getSelectedRow()+i,table.getSelectedColumn());
				}
			}
		if (e.getActionCommand() == "横表示"){
			for(int i=0;i<text.getText().length();i++){
				table.setValueAt(text.getText().substring(i, i+1),table.getSelectedRow(),table.getSelectedColumn()+i);
				}
			}
	
		//セル連結
		if (e.getActionCommand()=="セル連結"){
			
		}
		//セル解除
		if(e.getActionCommand()=="セル解除"){
			
		}
		//セーブ機能
		if (e.getActionCommand() == "セーブ"){
			
        }
		//ロード機能
		if (e.getActionCommand() == "ロード"){
			
        }
	}
}