package Excel;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.*;
//import java.util.InvalidPropertiesFormatException;
//import java.util.Map.Entry;

public class Excel extends JFrame implements ActionListener{
	private JFrame frame;
	private JTable table;
	private JTextField text,text1,text2,text3;
	private JScrollPane sp;
	private JPanel p;
	private int flag;
	private int row1,col1,row2,col2;
	private int ID=0,X=0,Y=0,width=0,height=0,parentID=0;//保存形式変数の初期化
	private String name,value;
	private String data[]={"name","value","ID","x","y","width","height","parentID"};
	
	public static void main(String[] args){
		Excel Excel = new Excel("Excel方眼紙");
		Excel.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Excel.setVisible(true);
	}
	public Excel(String title){
		setTitle(title);
		setBounds(50,0,1175,750);//表示位置(x,y)とウィンドウサイズ(width,height)

		//パネル生成
		p = new JPanel();
		//p.setLayout(null);

		//メニューバー作成
		JMenuBar menubar = new JMenuBar();
	    JMenu file = new JMenu("ファイル");
	    JMenu edit = new JMenu("編集");
	    JMenu display = new JMenu("表示");
	    JMenu help = new JMenu("ヘルプ");
	    JMenu deconcatenation = new JMenu("連結・解除");
	    JMenu item = new JMenu("アイテム");
	    JMenuItem save = new JMenuItem("セーブ");
	    JMenuItem load = new JMenuItem("ロード");
	    JMenuItem verticaldisplay = new JMenuItem("縦書き表示");
	    JMenuItem horizontaldisplay = new JMenuItem("横書き表示");
	    JMenuItem adisplay = new JMenuItem("斜め表示");
	    JMenuItem cellconnected = new JMenuItem("セル連結");
	    JMenuItem cellrelease = new JMenuItem("セル解除");
	    JMenuItem ColorCoding = new JMenuItem("色分け");
	    JMenuItem checkbox = new JMenuItem("チェックボックス");
	    JMenuItem rangespecification = new JMenuItem("範囲指定");
	    JMenuItem version=new JMenuItem("バージョン情報");

	    //メニューバー表示
	    setJMenuBar(menubar);
	    menubar.add(file);
	    menubar.add(edit);
	    menubar.add(help);
	    file.add(save);
	    file.add(load);
	    edit.add(display);
	    edit.add(deconcatenation);
	    edit.add(item);
	    display.add(verticaldisplay);
	    display.add(horizontaldisplay);
	    display.add(adisplay);
	    deconcatenation.add(cellconnected);
	    deconcatenation.add(cellrelease);
	    item.add(ColorCoding);
	    item.add(checkbox);
	    item.add(rangespecification);
	    help.add(version);

	    //メニューバー監視
	    save.addActionListener(this);
	    load.addActionListener(this);
	    verticaldisplay.addActionListener(this);
	    horizontaldisplay.addActionListener(this);
	    adisplay.addActionListener(this);
	    cellconnected.addActionListener(this);
	    cellrelease.addActionListener(this);
	    ColorCoding.addActionListener(this);
	    checkbox.addActionListener(this);
	    rangespecification.addActionListener(this);
	    version.addActionListener(this);
	    
	    //セル作成
		table = new JTable(200,200);//セル個数
		table.setFont(new Font("Arial", Font.PLAIN, 12));//フォント指定
		//table.setBounds(100, 0, 100, 700);
		table.setRowSelectionAllowed(true);//セルの行の選択状態を変更
		table.setColumnSelectionAllowed(true);//セルの列の選択状態を変更
		//table.setGridColor(Color.BLACK);//グリット線
		//table.setBorder(BorderFactory.createLineBorder(Color.BLACK));//枠生成
		
		/*
		DefaultTableModel tableModel
	     = new DefaultTableModel(0,0);
		*/
		
		//テキストフィールド作成
	    text = new JTextField(40);//テキストフィールド作成
	    text1 = new JTextField(10);//テキストフィールド作成
	    text2 = new JTextField(10);//テキストフィールド作成

	    text.setFont(new Font("Arial", Font.PLAIN, 12));//フォント指定
	    text1.setFont(new Font("Arial", Font.PLAIN, 12));//フォント指定
	    text2.setFont(new Font("Arial", Font.PLAIN, 12));//フォント指定

	    text.setHorizontalAlignment(JTextField.LEFT);//テキストの文字列内配置指定
	    text1.setHorizontalAlignment(JTextField.LEFT);//テキストの文字列内配置指定
	    text2.setHorizontalAlignment(JTextField.LEFT);//テキストの文字列内配置指定

	    //ボタン
	    JButton button = new JButton("セル入力");
	    JButton button1 = new JButton("属性入力");
	    JButton button2 = new JButton("属性名非表示");
	    JButton button3 = new JButton("属性値入力");

	    //ボタン監視
	    button.addActionListener(this);
	    button1.addActionListener(this);
	    button2.addActionListener(this);
	    button3.addActionListener(this);

	    //セル幅指定
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	    DefaultTableColumnModel columnModel
	    = (DefaultTableColumnModel)table.getColumnModel();

	    TableColumn column=null;
	    for (int i=0;i<columnModel.getColumnCount();i++){
	    column = columnModel.getColumn(i);
	    column.setPreferredWidth(3);
	    }

	    //ラベル作成
	    JLabel zokusei = new JLabel("属性:");
	    JLabel zokuseiti = new JLabel("属性値:");

	    //table.setDefaultRenderer(Object.class, new CellRenderer());

		sp = new JScrollPane(table);
	    sp.setPreferredSize(new Dimension(775,650));//フレームサイズ(x,y)

	    p.add(sp);
	    p.add(text);
	    p.add(button);
	    p.add(zokusei);
	    p.add(text1);
	    p.add(button1);
	    p.add(button2);
	    p.add(zokuseiti);
	    p.add(text2);
	    p.add(button3);

	    Container contentPane = getContentPane();
	    //contentPane.add(p, BorderLayout.CENTER);
	    contentPane.add(p);
	}

	public void actionPerformed(ActionEvent e) {
		
		//セーブ機能
		//セーブ方法：XML
		//セーブ内容：(ID,X,Y,height,width,parentID)
		if (e.getActionCommand() == "セーブ"){
			try{
				//ドキュメントビルダーファクトリを生成
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				//ドキュメントビルダーを生成
				DocumentBuilder builder = factory.newDocumentBuilder();
				//Documentオブジェクトを取得
				Document document = builder.newDocument();
				//ルート要素を追加する
				Element rootElement = document.createElement("savedata");
				document.appendChild(rootElement);
				rootElement.setAttribute("name","name");
	        
				//子要素を追加する
				Element element = document.createElement("Box");
				//Element element1 = document.createElement("value");
				//Element element2 = document.createElement("ID");
				//Element element3 = document.createElement("x");
				//Element element4 = document.createElement("y");
				//Element element5 = document.createElement("width");
				//Element element6 = document.createElement("height");
				//Element element7 = document.createElement("parentID");
				rootElement.appendChild(element);
				//rootElement.appendChild(element1);
				//rootElement.appendChild(element2);
				//rootElement.appendChild(element3);
				//rootElement.appendChild(element4);
				//rootElement.appendChild(element5);
				//rootElement.appendChild(element6);
				//rootElement.appendChild(element7);
				
				//値を設定する
				Text textContents = document.createTextNode("Box");
				Text textContents1 = document.createTextNode("value");
				Text textContents2 = document.createTextNode("ID");
				Text textContents3 = document.createTextNode("x");
				Text textContents4 = document.createTextNode("y");
				Text textContents5 = document.createTextNode("width");
				Text textContents6 = document.createTextNode("height");
				Text textContents7 = document.createTextNode("parentID");
				element.appendChild(textContents);
				//element1.appendChild(textContents1);
				//element2.appendChild(textContents2);
				//element3.appendChild(textContents3);
				//element4.appendChild(textContents4);
				//element5.appendChild(textContents5);
				//element6.appendChild(textContents6);
				//element7.appendChild(textContents7);
				//属性を追加する
				element.setAttribute("name","name");
	        
				File f =new File("savedata.xml"); 
				FileOutputStream fos = new FileOutputStream(f); 
				StreamResult result = new StreamResult(fos); 
	        
				// Transformerファクトリを生成
				TransformerFactory transFactory = TransformerFactory.newInstance();
				// Transformerを取得
				Transformer transformer = transFactory.newTransformer(); 
	        
				// エンコード：UTF-8、インデントありを指定
				transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	        
				// transformerに渡すソースを生成
				DOMSource source = new DOMSource(document);
	        
				//出力実行
				transformer.transform(source, result); 
				fos.close(); 
			}
			catch(Exception ex){
				ex.printStackTrace() ;
			}
		}

		//ロード機能
		if (e.getActionCommand() == "ロード"){
			try{
				File file = new File("/Users/sugayaryota/Documents/workspace2/Excel/kyuzin2.xml");
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); // ドキュメントビルダーファクトリを生成
				DocumentBuilder documentBuilder = factory.newDocumentBuilder(); // ドキュメントビルダーを生成
				Document document = documentBuilder.parse(file);// パースを実行してDocumentオブジェクトを取得
				Element root = document.getDocumentElement(); // ルート要素を取得

				//ルート要素のノード名を取得する
				System.out.println("ノード名：" +root.getNodeName());

				//ルート要素の属性を取得する
				System.out.println("ルート要素の属性：" + root.getAttribute("name"));

				//ルート要素の子ノードを取得する
				NodeList rootChildren = root.getChildNodes();

				System.out.println("子要素の数：" + rootChildren.getLength());
				System.out.println("------------------");
				
				//XMLの中身取得
				for(int i=0; i < rootChildren.getLength(); i++) {
					Node node = rootChildren.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element)node;
						if (element.getNodeName().equals("box")) {
							System.out.println("name:" + element.getAttribute(data[0]));
							name=element.getAttribute(data[0]);
							NodeList NodeChildren = node.getChildNodes();
							for (int j=0; j < NodeChildren.getLength(); j++) {
								Node BoxNode = NodeChildren.item(j);
								if (BoxNode.getNodeType() == Node.ELEMENT_NODE) {
									/*
									if (BoxNode.getNodeName().equals(data[0])) {
										name=BoxNode.getTextContent();
										System.out.println("name:" + BoxNode.getTextContent());
									}
									*/
									if (BoxNode.getNodeName().equals(data[1])) {
										value=BoxNode.getTextContent();
										System.out.println("value:" + BoxNode.getTextContent());
									}
									else if (BoxNode.getNodeName().equals(data[2])) {
										ID = Integer.parseInt(BoxNode.getTextContent());
										System.out.println("ID:" + BoxNode.getTextContent());
									}
									else if (BoxNode.getNodeName().equals(data[3])) {
										X = Integer.parseInt(BoxNode.getTextContent());
										System.out.println("x:" + BoxNode.getTextContent());
									}
									else if (BoxNode.getNodeName().equals(data[4])) {
										Y = Integer.parseInt(BoxNode.getTextContent());
										System.out.println("y:" + BoxNode.getTextContent());
									}
									else if (BoxNode.getNodeName().equals(data[5])) {
										width = Integer.parseInt(BoxNode.getTextContent());
										System.out.println("width:" + BoxNode.getTextContent());
									}
									else if (BoxNode.getNodeName().equals(data[6])) {
										height = Integer.parseInt(BoxNode.getTextContent());
										System.out.println("height:" + BoxNode.getTextContent());
									}
									else if (BoxNode.getNodeName().equals(data[7])) {
										parentID = Integer.parseInt(BoxNode.getTextContent());
										System.out.println("parentID:" + BoxNode.getTextContent());
									}
								}
							}
							System.out.println("------------------");
							//tableに文字を挿入
									
								
							for(int a=0;a<name.length();a++){
								if(name.length()>=1){
									table.setValueAt(name.substring(a,a+1),X,Y+a);
									//table.setBackground(Color.yellow);
								}
								else{break;}
							}
							System.out.println("nameを出力しました");
							
							for(int a=0;a<value.length();a++){
								if(value.length()>=1){
									table.setValueAt(value.substring(a,a+1),X,Y+a+name.length()+1);
									//table.setBackground(Color.yellow);
								}
								else{break;}
							}
							System.out.println("valueを出力しました");
						}
					}
				}				
			}
			catch(Exception ex){
				ex.printStackTrace() ;
			}
		}

		//セル入力(ボタン)
		if (e.getActionCommand()=="セル入力"){
			for(int i=0;i<text.getText().length();i++){
				//セルに文字が追加される
				//デフォルトでは横に表示される
				table.setValueAt(text.getText().substring(i, i+1),table.getSelectedRow(),table.getSelectedColumn()+i);
			}
		}

		//属性入力(ボタン)
		if (e.getActionCommand()=="属性入力"){
			for(int i=0;i<text1.getText().length();i++){
				//セルに文字が追加される
				//デフォルトではに表示される
				table.setValueAt(text1.getText().substring(i, i+1),table.getSelectedRow(),table.getSelectedColumn()+i);
			}
		}

		//属性名非表示(ボタン)
		if (e.getActionCommand()=="属性名非表示"){
			//textの中の文字列を非表示にする

		}

		//属性値入力(ボタン)
		if (e.getActionCommand()=="属性値入力"){
			for(int i=0;i<text2.getText().length();i++){
				//セルに文字が追加される
				//デフォルトでは横に表示される
				table.setValueAt(text2.getText().substring(i, i+1),table.getSelectedRow(),table.getSelectedColumn()+i);
			}
		}

		//縦横変換
		//縦表示
		if (e.getActionCommand() == "縦書き表示"){
			for(int i=0;i<text.getText().length();i++){
				//セルに文字が縦に追加される
				table.setValueAt(text.getText().substring(i, i+1),table.getSelectedRow()+i,table.getSelectedColumn());
			}

		}
		//横表示
		if (e.getActionCommand() == "横書き表示"){
			for(int i=0;i<text.getText().length();i++){
				//セルに文字が横に追加される
				table.setValueAt(text.getText().substring(i, i+1),table.getSelectedRow(),table.getSelectedColumn()+i);
			}
		}
		
		//斜め表示
		if (e.getActionCommand() == "斜め表示"){
			for(int i=0;i<text.getText().length();i++){
				//セルに文字が横に追加される
				table.setValueAt(text.getText().substring(i, i+1),table.getSelectedRow()+i,table.getSelectedColumn()+i);
			}
		}

		//セル連結
		if (e.getActionCommand()=="セル連結"){
			table.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					//クリック処理 2回クリックさせる
					if(flag==0){
						//1回目:セルの座標取得
						//選択セルの座標を取得する
						row1 = table.getSelectedRow();//行の選択
						col1 = table.getSelectedColumn();//列の選択
						System.out.println("1回目:"+"行" + row1 + "::" + "列" + col1);
						//flag=1;
					}
					else{
						//2回目:セルの座標取得
						// 選択セルの座標を取得する
						row2 = table.getSelectedRow();//行の選択
						col2 = table.getSelectedColumn();//列の選択
						System.out.println("2回目:"+"行" + row2 + "::" + "列" + col2);
						flag=0;
					}
					if(flag==0){
						int row=row2-row1;//個数を計算
						int col=col2-col1;//個数を計算
						//System.out.println(row+"::"+col);//個数表示
						for(int r=row1;r<=row1+row;r++){
							for(int c=col1;c<=col1+col;c++){
								//System.out.println(r+"::"+c);//座標表示
								//セルを連結させる
								
							}
						}
					}
				}
			});
		}

		//セル解除
		if(e.getActionCommand()=="セル解除"){
			table.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					//クリック処理 2回クリックさせる
					if(flag==0){
						//1回目:セルの座標取得
						//選択セルの座標を取得する
						row1 = table.getSelectedRow();//行の選択
						col1 = table.getSelectedColumn();//列の選択
						System.out.println("1回目:"+"行" + row1 + "::" + "列" + col1);
						flag=1;
					}else{
						//2回目:セルの座標取得
						// 選択セルの座標を取得する
						row2 = table.getSelectedRow();//行の選択
						col2 = table.getSelectedColumn();//列の選択
						System.out.println("2回目:"+"行" + row2 + "::" + "列" + col2);
						flag=0;
					}
					if(flag==0){
						int row=row2-row1;//個数を計算
						int col=col2-col1;//個数を計算
						//System.out.println(row+"::"+col);//個数表示
						for(int r=row1;r<=row+row1;r++){
							for(int c=col1;c<=col+col1;c++){
								//System.out.println(r+"::"+c);//座標表示
								//連結したセルを解除させる
							}
						}
					}
				}
			});
		}

		//罫線からセルの塊の色分けに変更
		if (e.getActionCommand() == "色分け"){
			//範囲指定する
			//テーブルにマウスイベントをつけて監視する
			table.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					//クリック処理 2回クリックさせる
					if(flag==0){
						//1回目:セルの座標取得
						//選択セルの座標を取得する
						row1 = table.getSelectedRow();//行の選択
						col1 = table.getSelectedColumn();//列の選択
						//System.out.println("1回目:"+"行" + row1 + "::" + "列" + col1);//1回目のセルの座標を表示
						flag=1;
						}else{
							//2回目:セルの座標取得
							// 選択セルの座標を取得する
							row2 = table.getSelectedRow();//行の選択
							col2 = table.getSelectedColumn();//列の選択
							//System.out.println("2回目:"+"行" + row2 + "::" + "列" + col2);//2回目のセルの座標を表示
							flag=0;
						}
					if(flag==0){
						//int row=row2-row1;//個数を計算
						//int col=col2-col1;//個数を計算
						//System.out.println(row+"::"+col);//個数表示
						for(int r=row1;r<=row2;r++){
							for(int c=col1;c<=col2;c++){
								System.out.println(r+"::"+c);//セルの座標表示
								//色分け
								table.setBackground(Color.RED);
							}
						}
					}
				}
			});
		}

		//チェックボックス機能
		if (e.getActionCommand() == "チェックボックス"){
			//セルの座標取得
			table.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					// 選択セルの座標を取得する
					int row = table.getSelectedRow();
					int col = table.getSelectedColumn();
					//System.out.println("行" + row + "::" + "列" + col);
					//セルにチェックボックスを表示させる
					table.setValueAt("✔",row,col);
				   }
			});
        }

		//範囲指定
		if (e.getActionCommand() == "範囲指定"){
			table.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					//クリック処理 2回クリックさせる
					if(flag==0){
						//1回目:セルの座標取得
						//選択セルの座標を取得する
						row1 = table.getSelectedRow();//行の選択
						col1 = table.getSelectedColumn();//列の選択
						System.out.println("1回目:"+"行" + row1 + "::" + "列" + col1);
						flag=1;
					}
					else{
						//2回目:セルの座標取得
						//選択セルの座標を取得する
						row2 = table.getSelectedRow();//行の選択
						col2 = table.getSelectedColumn();//列の選択
						System.out.println("2回目:"+"行" + row2 + "::" + "列" + col2);
						flag=0;
					}
					if(flag==0){
						//row1→row2,col1→col2
						/*例：
						 * 行10,列10(1回目)
						 * 行30,列40(2回目)
						 */
						if(row1<row2&&col1<col2){
							int row=row2-row1;//個数を計算
							int col=col2-col1;//個数を計算
							//System.out.println("row個数:"+row+"::"+"col個数:"+col);//個数表示
							for(int r=row1;r<=row+row1;r++){
								for(int c=col1;c<=col+col1;c++){
									System.out.println(Math.abs(r)+"::"+Math.abs(c));//座標表示
									//table.setBackground(Color.RED);//範囲指定した分だけ色を変える
									//table.setSelectionBackground(Color.RED);
								}
							}
						}
						//row1→row2,col1→col2
						/*例：
						 * 行30,列40(1回目)
						 * 行10,列10(2回目)
						 */
						/*
						else if(row1>row2&&col1>col2){
							int row=row1-row2;//個数を計算
							int col=col1-col2;//個数を計算
							System.out.println(row+"::"+col);//個数表示
							for(int r=row1;r<=row-row1;r++){
								for(int c=col1;c<=col-col1;c++){
									System.out.println(r+"::"+c);//座標表示
									//table.setBackground(Color.RED);//範囲指定した分だけ色を変える
									//table.setSelectionBackground(Color.RED);
								}
							}
						}
						*/
						//row1→row2,col1→col2
						/*例：
						 * 行10,列20(1回目)
						 * 行30,列10(2回目)
						 */
						/*
						else if(row1<row2&&col1>col2){
							int row=row2-row1;//個数を計算
							int col=col2-col1;//個数を計算
							System.out.println(Math.abs(row)+"::"+col);//個数表示
							for(int r=row1;r<=row-row1;r--){
								for(int c=col1;c<=col1+col;c--){
									System.out.println(r+"::"+c);//座標表示
									//table.setBackground(Color.RED);//範囲指定した分だけ色を変える
									//table.setSelectionBackground(Color.RED);
								}
							}
						}
						*/
						//row1→row2,col1→col2
						/*例：
						 * 行30,列10(1回目)
						 * 行10,列20(2回目)
						 */
						/*
						else if(row1>row2&&col1<col2){
							int row=row2-row1;//個数を計算
							int col=col2-col1;//個数を計算
							System.out.println(row+"::"+col);//個数表示
							for(int r=row1;r<=row+row1;r++){
								for(int c=col1;c<=col+col1;c++){
									System.out.println(r+"::"+c);//座標表示
									//table.setBackground(Color.RED);//範囲指定した分だけ色を変える
									//table.setSelectionBackground(Color.RED);
								}
							}
						}*/
					}
				}
			});
		}
		if(e.getActionCommand()=="バージョン情報"){
			Dialog alert = new Dialog(frame, "バージョン情報");
			alert.add(new Label("version:2015/12/21"));
			alert.setBounds(100,0,200,200);;
			alert.setVisible(true);
			alert.addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent e){System.exit(0);}});
		}
	}
}