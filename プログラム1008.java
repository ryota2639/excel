import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class Excel extends JFrame implements ActionListener{
	private JFrame frame;
	private JTable table;
	private JTextField text;
	private JScrollPane sp;
	private JPanel p;
	private JCheckBox ckbox = new JCheckBox();
	int i=0,flag;
	int row1,col1,row2,col2,row3,col3;
	
	public static void main(String[] args){
		Excel excel = new Excel("Excel");
		excel.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		excel.setVisible(true);
		}
	Excel(String title){
		setTitle(title);
		setBounds(0,0,800,800);//表示位置とウィンドウサイズ
		
		//メニューバー作成
		JMenuBar menubar = new JMenuBar();
	    JMenu menu1 = new JMenu("ファイル");
	    JMenu menu2 = new JMenu("編集");
	    JMenu submenu1 = new JMenu("表示");
	    JMenu submenu2 = new JMenu("連結・解除");
	    JMenu submenu3 = new JMenu("アイテム");
	    JMenuItem menuitem1 = new JMenuItem("セーブ");
	    JMenuItem menuitem2 = new JMenuItem("ロード");
	    JMenuItem submenuitem1 = new JMenuItem("縦表示");
	    JMenuItem submenuitem2 = new JMenuItem("横表示");
	    JMenuItem submenuitem3 = new JMenuItem("セル連結");
	    JMenuItem submenuitem4 = new JMenuItem("セル解除");
	    JMenuItem submenuitem5 = new JMenuItem("罫線作成");
	    JMenuItem submenuitem6 = new JMenuItem("チェックボックス");
	    JMenuItem submenuitem7 = new JMenuItem("範囲指定");
	    
	    //メニューバー表示
	    setJMenuBar(menubar);
	    menubar.add(menu1);
	    menubar.add(menu2);
	    menu1.add(menuitem1);
	    menu1.add(menuitem2);
	    menu2.add(submenu1);
	    menu2.add(submenu2);
	    menu2.add(submenu3);
	    submenu1.add(submenuitem1);
	    submenu1.add(submenuitem2);
	    submenu2.add(submenuitem3);
	    submenu2.add(submenuitem4);
	    submenu3.add(submenuitem5);
	    submenu3.add(submenuitem6);
	    submenu3.add(submenuitem7);
	    
	    //メニューバー監視
	    menuitem1.addActionListener(this);
	    menuitem2.addActionListener(this);
	    submenuitem1.addActionListener(this);
	    submenuitem2.addActionListener(this);
	    submenuitem3.addActionListener(this);
	    submenuitem4.addActionListener(this);
	    submenuitem5.addActionListener(this);
	    submenuitem6.addActionListener(this);
	    submenuitem7.addActionListener(this);
	    
	    //セル作成
		table = new JTable(99,99);//セルサイズ
		table.setFont(new Font("Arial", Font.PLAIN, 12));//フォント指定
		table.setRowSelectionAllowed(true);//セルの行の選択状態を変更
		table.setColumnSelectionAllowed(true);//セルの列の選択状態を変更
		//table.setGridColor(Color.BLACK);//グリット線
		DefaultTableModel tableModel
	     = new DefaultTableModel(0,0);
		
		//テキストフィールド作成
	    text = new JTextField(40);//テキストフィールド作成
	    
	    text.setFont(new Font("Arial", Font.PLAIN, 12));//フォント指定
	    text.setHorizontalAlignment(JTextField.LEFT);//テキストの文字列内配置指定
	    
	    //ボタン
	    JButton button = new JButton("セル入力");
	    button.addActionListener(this);
	    	    
	    //セル幅指定
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	    DefaultTableColumnModel columnModel
	    = (DefaultTableColumnModel)table.getColumnModel();

	    TableColumn column=null;
	    for (int i=0;i<columnModel.getColumnCount();i++){
	    column = columnModel.getColumn(i);
	    column.setPreferredWidth(2);
	    }
	    
	    //チェックボックス機能
	    JCheckBox ckbox = new JCheckBox();
	    ckbox.addActionListener(this);
	    
		sp = new JScrollPane(table);
	    sp.setPreferredSize(new Dimension(650,650));//フレームサイズ(x,y)
	    
	    p = new JPanel();
	    p.add(sp);
	    p.add(text);
	    p.add(button);
	    //p.add(ckbox);

	    Container contentPane = getContentPane();
	    contentPane.add(p, BorderLayout.SOUTH);
	    getContentPane().add(p,BorderLayout.CENTER);
	}
	
	public void actionPerformed(ActionEvent e) {
		// TODO 自動生成されたメソッド・スタブ
		//セル入力
		if (e.getActionCommand()=="セル入力"){
			for(int i=0;i<text.getText().length();i++){
				table.setValueAt(text.getText().substring(i, i+1),table.getSelectedRow(),table.getSelectedColumn()+i);//セルに文字が追加される
				//デフォルトでは横に表示される
			}
		}
		
		//縦横変換
		//縦表示
		if (e.getActionCommand() == "縦表示"){
			for(int i=0;i<text.getText().length();i++){
				table.setValueAt(text.getText().substring(i, i+1),table.getSelectedRow()+i,table.getSelectedColumn());//セルに文字が横に追加される
			}
		}
		//横表示
		if (e.getActionCommand() == "横表示"){
			for(int i=0;i<text.getText().length();i++){
				table.setValueAt(text.getText().substring(i, i+1),table.getSelectedRow(),table.getSelectedColumn()+i);//セルに文字が縦に追加される
			}
		}
	
		//セル連結
		if (e.getActionCommand()=="セル連結"){
			table.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					//クリック処理 2回クリックさせる
					if(flag==0){
						//1回目:セルの座標取得
						//選択セルの座標を取得する
						row1 = table.getSelectedRow();
						col1 = table.getSelectedColumn();
						System.out.println("1回目:"+"行" + row1 + "::" + "列" + col1);
						flag=1;
					}else{
						//2回目:セルの座標取得
						// 選択セルの座標を取得する
						row2 = table.getSelectedRow();
						col2 = table.getSelectedColumn();
						System.out.println("2回目:"+"行" + row2 + "::" + "列" + col2);
						flag=0;
					}
					if(flag==0){
						int row=row2-row1;//個数を計算
						int col=col2-col1;//個数を計算
						//System.out.println(row+"::"+col);//個数表示
						for(int r=row1;r<=row1+row;r++){
							for(int c=col1;c<=col1+col;c++){
								//System.out.println(r+"::"+c);//座標表示
								//セルを連結させる
								
							}	
						}
					}
					}
				});
			
		}
		
		//セル解除
		if(e.getActionCommand()=="セル解除"){
			table.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					//クリック処理 2回クリックさせる
					if(flag==0){
						//1回目:セルの座標取得
						//選択セルの座標を取得する
						row1 = table.getSelectedRow();
						col1 = table.getSelectedColumn();
						System.out.println("1回目:"+"行" + row1 + "::" + "列" + col1);
						flag=1;
					}else{
						//2回目:セルの座標取得
						// 選択セルの座標を取得する
						row2 = table.getSelectedRow();
						col2 = table.getSelectedColumn();
						System.out.println("2回目:"+"行" + row2 + "::" + "列" + col2);
						flag=0;
					}
					if(flag==0){
						int row=row2-row1;//個数を計算
						int col=col2-col1;//個数を計算
						//System.out.println(row+"::"+col);//個数表示
						for(int r=row1;r<=row+row1;r++){
							for(int c=col1;c<=col+col1;c++){
								//System.out.println(r+"::"+c);//座標表示
								//連結したセルを解除させる
								
							}	
						}
					}
					}
				});
		}
		
		//セーブ機能
		if (e.getActionCommand() == "セーブ"){
			
        }
		
		//ロード機能
		if (e.getActionCommand() == "ロード"){
			
        }
		
		//罫線作成
		if (e.getActionCommand() == "罫線作成"){
			//範囲指定する
			//テーブルにマウスイベントをつけて監視する
			/*
			table.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					//クリック処理 2回クリックさせる
					if(flag==0){
						//1回目:セルの座標取得
						//選択セルの座標を取得する
						row1 = table.getSelectedRow();
						col1 = table.getSelectedColumn();
						System.out.println("1回目:"+"行" + row1 + "::" + "列" + col1);//1回目の座標を表示
						flag=1;
						}else{
							//2回目:セルの座標取得
							// 選択セルの座標を取得する
							row2 = table.getSelectedRow();
							col2 = table.getSelectedColumn();
							System.out.println("2回目:"+"行" + row2 + "::" + "列" + col2);//2回目の座標を表示
							flag=0;
						}
					if(flag==0){
						int row=row2-row1;//個数を計算
						int col=col2-col1;//個数を計算
						//System.out.println(row+"::"+col);//個数表示
						for(int r=row1;r<=row+row1;r++){
							for(int c=col1;c<=col+col1;c++){
								System.out.println(Math.abs(r)+"::"+Math.abs(c));//座標表示
								//罫線を作成する
							}		
						}
					}
				}
			});
			*/
			
			//セルの座標取得
			table.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					// 選択セルの座標を取得する
					int row = table.getSelectedRow();
					int col = table.getSelectedColumn();
					//System.out.println("行" + row + "::" + "列" + col);
					//セルに罫線を表示させる
					table.setValueAt("＼",row,col);
				   }
				  });
			
		}
		//チェックボックス機能
		if (e.getActionCommand() == "チェックボックス"){
			//セルの座標取得
			table.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					// 選択セルの座標を取得する
					int row = table.getSelectedRow();
					int col = table.getSelectedColumn();
					//System.out.println("行" + row + "::" + "列" + col);
					//セルにチェックボックスを表示させる
					table.setValueAt("✔",row,col);
				   }
				  });
        }
		
		//範囲指定
		if (e.getActionCommand() == "範囲指定"){
			table.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					//クリック処理 2回クリックさせる
					if(flag==0){
						//1回目:セルの座標取得
						//選択セルの座標を取得する
						row1 = table.getSelectedRow();
						col1 = table.getSelectedColumn();
						System.out.println("1回目:"+"行" + row1 + "::" + "列" + col1);
						flag=1;
						}else{
							//2回目:セルの座標取得
							//選択セルの座標を取得する
							row2 = table.getSelectedRow();
							col2 = table.getSelectedColumn();
							System.out.println("2回目:"+"行" + row2 + "::" + "列" + col2);
							flag=0;
						}
					if(flag==0){
						//row1→row2,col1→col2
						/*例：
						 * 行10,列10(1回目)
						 * 行30,列40(2回目)
						 */
						if(row1<row2&&col1<col2){
							int row=row2-row1;//個数を計算
							int col=col2-col1;//個数を計算
							//System.out.println("row個数:"+row+"::"+"col個数:"+col);//個数表示
							for(int r=row1;r<=row+row1;r++){
								for(int c=col1;c<=col+col1;c++){
									System.out.println(Math.abs(r)+"::"+Math.abs(c));//座標表示
									//table.setBackground(Color.RED);//範囲指定した分だけ色を変える
									//table.setSelectionBackground(Color.RED);
								}	
							}
						}
						//row1→row2,col1→col2
						/*例：
						 * 行30,列40(1回目)
						 * 行10,列10(2回目)
						 */
						/*
						else if(row1>row2&&col1>col2){
							int row=row1-row2;//個数を計算
							int col=col1-col2;//個数を計算
							System.out.println(row+"::"+col);//個数表示
							for(int r=row1;r<=row-row1;r++){
								for(int c=col1;c<=col-col1;c++){
									System.out.println(r+"::"+c);//座標表示
									//table.setBackground(Color.RED);//範囲指定した分だけ色を変える
									//table.setSelectionBackground(Color.RED);
								}	
							}
						}
						*/
						//row1→row2,col1→col2
						/*例：
						 * 行10,列20(1回目)
						 * 行30,列10(2回目)
						 */
						/*
						else if(row1<row2&&col1>col2){
							int row=row2-row1;//個数を計算
							int col=col2-col1;//個数を計算
							System.out.println(Math.abs(row)+"::"+col);//個数表示
							for(int r=row1;r<=row-row1;r--){
								for(int c=col1;c<=col1+col;c--){
									System.out.println(r+"::"+c);//座標表示
									//table.setBackground(Color.RED);//範囲指定した分だけ色を変える
									//table.setSelectionBackground(Color.RED);
								}	
							}
						}
						*/
						//row1→row2,col1→col2
						/*例：
						 * 行30,列10(1回目)
						 * 行10,列20(2回目)
						 */
						/*
						else if(row1>row2&&col1<col2){
							int row=row2-row1;//個数を計算
							int col=col2-col1;//個数を計算
							System.out.println(row+"::"+col);//個数表示
							for(int r=row1;r<=row+row1;r++){
								for(int c=col1;c<=col+col1;c++){
									System.out.println(r+"::"+c);//座標表示
									//table.setBackground(Color.RED);//範囲指定した分だけ色を変える
									//table.setSelectionBackground(Color.RED);
								}	
							}
						}*/
					}
				}
			});
		}	
	}
}